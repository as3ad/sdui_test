<?php

namespace App\Repositories;

interface NewsRepositoryInterface
{
    public function getAllNews();

    public function deleteById($newsId);

    public function getById($newsId);

    public function create($request);

    public function update($newsId, $request);

    public function deleteExpiredNews($numberOfDays);
}
