<?php


namespace App\Repositories;


use App\Models\News;
use Carbon\Carbon;

class NewsRepository implements NewsRepositoryInterface
{
    public function getAllNews()
    {
        return News::all();
    }

    public function deleteById($newsId)
    {
        return News::find($newsId)->delete();
    }

    public function getById($newsId)
    {
        return News::find($newsId);
    }

    public function create($request)
    {
        return News::create($request);
    }

    public function update($newsId, $request)
    {
        return News::find($newsId)->update($request);
    }

    public function deleteExpiredNews($numberOfDays)
    {
        return News::where('created_at', '<=', Carbon::now()->subDays($numberOfDays))->delete();
    }


}

