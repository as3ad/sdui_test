<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AssignUserToNews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        if ($user) {
            $request->merge(['user_id' => $user->id]);
        }
        return $next($request);
    }
}
