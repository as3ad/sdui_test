<?php

namespace App\Http\Controllers;

use App\Events\NewsCreated;
use App\Http\Requests\NewsRequest;
use App\Repositories\NewsRepositoryInterface;
use App\Model\News;
use mysql_xdevapi\Exception;

class NewsController extends Controller
{
    private $newsRepository;

    public function __construct(NewsRepositoryInterface $newsRepository)
    {
        $this->middleware('assign.user.to.news')->only('store', 'update');
        $this->newsRepository = $newsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->response('success', $this->newsRepository->getAllNews());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\NewsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request)
    {
        try {
            $news = $this->newsRepository->create($request->all());
            // event Should be in serves but in task file you mention that Should fire from controller .
            if ($news) {
                event(new NewsCreated($news));
            }
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $this->response('News created successfully.', $news, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response('success', $this->newsRepository->getById($id));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\NewsRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsRequest $request, $id)
    {
        try {
            $news = $this->newsRepository->update($id, $request->all());
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
        return $this->response('News updated successfully.', $news);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->response('News deleted successfully.', $this->newsRepository->deleteById($id));
    }
}
