# Sdui Test

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for review purposes.

### Prerequisites

To run this project you need to install [Composer](https://getcomposer.org/)

### Installing

1-  First of all create new SQLite database by creating a new file in database directory with name database.sqlite .

2-  Then in root directory rename `.env.example` to `.env` and initialize it.


```
composer install
```
```
composer dump-autoload
```
```
php artisan migrate
```
```
php artisan db:seed 
```


## Background Tasks

A schedule must always be working in background locally you can do :


```
php artisan schedule:run 

