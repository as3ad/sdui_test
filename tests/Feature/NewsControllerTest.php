<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use App\Models\User;
use App\Models\News;
use Tests\TestCase;

class NewsControllerTest extends TestCase
{
    use  WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        // create a user
        $this->user = User::factory()->create();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_see_list_news()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $response = $this->getJson(route('news.index'));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'title',
                        'content',
                        'user_id'
                    ]
                ]
            ]);;

    }

    public function test_can_create_news()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $data = [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->paragraph()
        ];

        $response = $this->postJson(route('news.store'), $data);

        $response->assertStatus(201)
            ->assertCreated()
            ->assertJson(['message' => 'News created successfully.']);
    }

    public function test_can_show_news()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $news = News::factory()->create();

        $response = $this->getJson(route('news.show', $news->id));

        $response->assertOk()
            ->assertJson([
                'message' => 'success',
                'data' =>
                    [
                        'id' => $news->id,
                        'title' => $news->title,
                        'content' => $news->content,
                        'user_id' => $news->user_id
                    ]
            ]);
    }

    public function test_can_update_news()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $news = News::factory()->create();

        $data = [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->paragraph()
        ];

        $response = $this->putJson(route('news.update', $news->id), $data);

        $response->assertOk()
            ->assertJson(['message' => 'News updated successfully.']);
    }

    public function test_can_delete_news()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $news = News::factory()->create();

        $response = $this->deleteJson(route('news.destroy', $news->id));

        $response->assertOk()
            ->assertJson(['message' => 'News deleted successfully.']);
    }

}
